# (Microservicio Pragpooling-Service-) Implementing Clean Architecture.

## Environment:

- Plugin Clean Architecture: https://github.com/bancolombia/scaffold-clean-architecture  It's just a reference.
- Java version: 8
- SpringBootVersion: 2.4.2
- Gradle version: 6.9
- Intellij Idea (Import the Project)


## Microservice Architecture Design:
![Clean Architecture](https://miro.medium.com/max/1400/1*ZdlHz8B0-qu9Y-QO3AXR_w.png)


## Data:
Example of a Script using Postman:

![Download Test Plan](https://gitlab.com/yonhbu/pragpooling-service/-/blob/main/Power_up_Java_x_AWS.postman_collection.json)


## Requirements 1: Login and Registration Project

URL: http://localhost:8000/

The `REST` service expose the `/api/v1/auth` endpoint, which allows for managing the security:

POST request to `/signup`:

- Register a new user

POST request to `/login`:

- Authenticate the new user



## Note:

It is necessary to send in the headers the token returned when logging in.
Authorization + Bearer token


## Requirements 2:

URL: http://localhost:8000/

The `REST` service expose the `/api/v1/user` endpoint, which allows for managing the collection of Customer records in the following way:


GET request to `/retrieve`:

- the response code is 200
- the response body is an array of matching records, ordered by their ids in increasing order


GET request to `/email/{email}`:

- the response code is 200
- the body of the response is a matching record object searched by the user's email address


## Requirements 3:

The `REST` service expose the `/api/v1/district` endpoint, which allows for managing the collection of District records in the following way:


POST request to `/save`:
GET request to `/retrieve`:


## Requirements 4:

The `REST` service expose the `/api/v1/route` endpoint, which allows for managing the collection of Route records in the following way:


GET request to `/retrieve`:


## Requirements 5:

The `REST` service expose the `/api/v1/journey` endpoint, which allows for managing the collection of Journey records in the following way:


POST request to `/save/driver`:
GET request to `/retrieve/driver`:


## Coverage Unit Tests:

To know the percentage of unit tests of the whole project run the following command `gradle jacocoMergedReport` from a CMD or Git console opened from the root of the project

