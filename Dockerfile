FROM openjdk:8-jdk-alpine
EXPOSE 8089
ADD build/*.jar app.jar
ENTRYPOINT java -jar -Duser.language=es -Duser.region=CO -Duser.timezone="America/Bogota" app.jar