package co.com.pragpooling.api.user;

import co.com.pragpooling.api.util.MapStructMapperAPI;
import co.com.pragpooling.model.user.dto.UserRsDTO;
import co.com.pragpooling.usecase.user.UserUseCase;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;


@RestController
@RequestMapping(value = "/api/v1/user", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(tags = {"User Service"})
@RequiredArgsConstructor
public class UserRestController {

    private final UserUseCase userUseCase;
    private final MapStructMapperAPI mapstructMapper;


    @GetMapping("/retrieve")
    public ResponseEntity<List<UserRsDTO>> retrieveAllUser () {

        List<UserRsDTO> userListResponse = mapstructMapper.mapAllUserModel(userUseCase.retrieveAllUser());
        return new ResponseEntity<>(userListResponse, HttpStatus.OK);

    }


    @GetMapping("/email/{email}")
    public ResponseEntity<UserRsDTO> getUserByEmail (@PathVariable ("email") String email) {

        UserRsDTO userResponse = mapstructMapper.userRsDTOToUserModel(userUseCase.getUserByEmail (email));
        return new ResponseEntity<>(userResponse, HttpStatus.CREATED);

    }
}
