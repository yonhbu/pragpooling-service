package co.com.pragpooling.api.route;

import co.com.pragpooling.api.util.MapStructMapperAPI;
import co.com.pragpooling.model.route.dto.RouteRsDTO;
import co.com.pragpooling.usecase.route.RouteUseCase;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping(value = "/api/v1/route", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(tags = {"Route Service"})
@RequiredArgsConstructor
public class RouteRestController {

    private final RouteUseCase routeUseCase;
    private final MapStructMapperAPI mapstructMapper;


    @GetMapping("/retrieve")
    public ResponseEntity<List<RouteRsDTO>> retrieveAllRoute () {

        List<RouteRsDTO> routeResponse = mapstructMapper.mapAllRouteModel(routeUseCase.retrieveAllRoute());
        return new ResponseEntity<>(routeResponse, HttpStatus.OK);

    }


}
