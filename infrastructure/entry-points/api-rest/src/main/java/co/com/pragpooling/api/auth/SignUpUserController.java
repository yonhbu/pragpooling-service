package co.com.pragpooling.api.auth;


import co.com.pragpooling.api.util.MapStructMapperAPI;
import co.com.pragpooling.model.auth.dto.SignUpUserRqDTO;
import co.com.pragpooling.model.auth.dto.SignUpUserRsDTO;
import co.com.pragpooling.usecase.auth.SignUpUserUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v1/auth", produces = MediaType.APPLICATION_JSON_VALUE)
public class SignUpUserController {

	private final SignUpUserUseCase signUpUserUseCase;
	private final MapStructMapperAPI mapstructMapper;


	@PostMapping("/signup")
	public ResponseEntity<SignUpUserRsDTO> signUpUser (@Valid @RequestBody SignUpUserRqDTO signUpUserRqDTO) {

		SignUpUserRsDTO signUpUserRsDTO = mapstructMapper.signUpUserRsDTOToSignUpStatusUserModel(
				signUpUserUseCase.signUpUser(mapstructMapper.signUpUserModelToSignUpRqDTO(signUpUserRqDTO)));

		return new ResponseEntity<>(signUpUserRsDTO, HttpStatus.CREATED);
	}
}