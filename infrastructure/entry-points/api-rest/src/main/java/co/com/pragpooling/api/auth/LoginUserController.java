package co.com.pragpooling.api.auth;


import co.com.pragpooling.api.util.MapStructMapperAPI;
import co.com.pragpooling.model.auth.dto.LoginUserRqDTO;
import co.com.pragpooling.model.auth.dto.TokenUserRsDTO;
import co.com.pragpooling.usecase.auth.LoginUserUseCase;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v1/auth", produces = MediaType.APPLICATION_JSON_VALUE)
public class LoginUserController {

	private final LoginUserUseCase loginUserUseCase;
	private final MapStructMapperAPI mapstructMapper;


	@PostMapping("/login")
	public ResponseEntity<TokenUserRsDTO> loginUser (@Valid  @RequestBody LoginUserRqDTO loginUserRqDTO) {

		TokenUserRsDTO tokenUserRsDTO = mapstructMapper.tokenUserRsDTOToTokenAuthenticateUserModel(
										loginUserUseCase.loginUser(mapstructMapper.loginModelToLoginRqDTO(loginUserRqDTO)));

		return new ResponseEntity<>(tokenUserRsDTO, HttpStatus.OK);

	}


}