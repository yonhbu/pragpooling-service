package co.com.pragpooling.api.util;


import co.com.pragpooling.model.auth.LoginUserModel;
import co.com.pragpooling.model.auth.SignUpStatusUserModel;
import co.com.pragpooling.model.auth.SignUpUserModel;
import co.com.pragpooling.model.auth.TokenAuthenticateUserModel;
import co.com.pragpooling.model.auth.dto.LoginUserRqDTO;
import co.com.pragpooling.model.auth.dto.SignUpUserRqDTO;
import co.com.pragpooling.model.auth.dto.SignUpUserRsDTO;
import co.com.pragpooling.model.auth.dto.TokenUserRsDTO;
import co.com.pragpooling.model.district.DistrictModel;
import co.com.pragpooling.model.district.dto.DistrictModelRqDTO;
import co.com.pragpooling.model.district.dto.DistrictModelRsDTO;
import co.com.pragpooling.model.journey.JourneyModel;
import co.com.pragpooling.model.journey.dto.JourneyRqDTO;
import co.com.pragpooling.model.journey.dto.JourneyRsDTO;
import co.com.pragpooling.model.route.RouteModel;
import co.com.pragpooling.model.route.dto.RouteRqDTO;
import co.com.pragpooling.model.route.dto.RouteRsDTO;
import co.com.pragpooling.model.user.UserModel;
import co.com.pragpooling.model.user.dto.UserRqDTO;
import co.com.pragpooling.model.user.dto.UserRsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(
        componentModel = "spring"
)
public interface MapStructMapperAPI {


    @Mapping(target = "idUser", ignore = true)
    UserModel userModelToUserRqDTO (UserRqDTO userRqDTO);

    UserRsDTO userRsDTOToUserModel(UserModel userModel);
    List<UserRsDTO> mapAllUserModel (List<UserModel> userModels);

    @Mapping(target = "idRoute", ignore = true)
    RouteModel routeModelToRouteRqDTO (RouteRqDTO routeModels);

    List<RouteRsDTO> mapAllRouteModel (List<RouteModel> routeModels);

    DistrictModel districtModelToDistrictRqDTO (DistrictModelRqDTO districtModelRqDTO);
    DistrictModelRsDTO districtRsDTOToDistrictModel(DistrictModel districtModel);

    List<DistrictModelRsDTO> mapAllDistrictModel (List<DistrictModel> districtModels);

    LoginUserModel loginModelToLoginRqDTO (LoginUserRqDTO loginUserRqDTO);
    TokenUserRsDTO tokenUserRsDTOToTokenAuthenticateUserModel(TokenAuthenticateUserModel tokenAuthenticateUserModel);

    @Mapping(target = "idSignup", ignore = true)
    SignUpUserModel signUpUserModelToSignUpRqDTO (SignUpUserRqDTO signUpUserRqDTO);
    SignUpUserRsDTO signUpUserRsDTOToSignUpStatusUserModel(SignUpStatusUserModel signUpStatusUserModel);

    @Mapping(target = "idJourney", ignore = true)
    @Mapping(target = "numberStops", ignore = true)
    JourneyModel journeyModelToJourneyRqDTO (JourneyRqDTO journeyRqDTO);

    List<JourneyRsDTO> mapAllJourneyModel (List<JourneyModel> journeyModel);



}