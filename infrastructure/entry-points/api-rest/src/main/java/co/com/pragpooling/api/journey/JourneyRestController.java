package co.com.pragpooling.api.journey;

import co.com.pragpooling.api.util.MapStructMapperAPI;
import co.com.pragpooling.model.journey.dto.JourneyRqDTO;
import co.com.pragpooling.model.journey.dto.JourneyRsDTO;
import co.com.pragpooling.usecase.journey.JourneyUseCase;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping(value = "/api/v1/journey", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(tags = {"Journey Service"})
@RequiredArgsConstructor
public class JourneyRestController {

    private final JourneyUseCase journeyUseCase;
    private final MapStructMapperAPI mapstructMapper;

    @PostMapping("/save/driver")
    public ResponseEntity<String> saveJourneyDriver (@Valid @RequestBody JourneyRqDTO journeyRqDTO) {

        journeyUseCase.saveJourneyDriver(mapstructMapper.journeyModelToJourneyRqDTO(journeyRqDTO));

        return ResponseEntity.ok("¡Successful Registration! Your Journey has been registered");

    }

    @GetMapping("/retrieve/driver")
    public ResponseEntity<List<JourneyRsDTO>> retrieveAllJourneyDriver () {

        List<JourneyRsDTO> journeyListResponse = mapstructMapper.mapAllJourneyModel(journeyUseCase.retrieveAllJourneyDriver());
        return new ResponseEntity<>(journeyListResponse, HttpStatus.OK);

    }

}
