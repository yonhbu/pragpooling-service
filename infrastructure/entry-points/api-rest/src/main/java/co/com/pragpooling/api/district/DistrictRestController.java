package co.com.pragpooling.api.district;

import co.com.pragpooling.api.util.MapStructMapperAPI;
import co.com.pragpooling.model.district.dto.DistrictModelRqDTO;
import co.com.pragpooling.model.district.dto.DistrictModelRsDTO;
import co.com.pragpooling.usecase.district.DistrictUseCase;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(value = "/api/v1/district", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(tags = {"District Service"})
@RequiredArgsConstructor
public class DistrictRestController {

    private final DistrictUseCase districtUseCase;
    private final MapStructMapperAPI mapstructMapper;


    @PostMapping("/save")
    public ResponseEntity<DistrictModelRsDTO> saveDistrict (@RequestBody DistrictModelRqDTO districtModelRqDTO) {

        DistrictModelRsDTO districtResponse = mapstructMapper.districtRsDTOToDistrictModel(
                districtUseCase.saveDistrict (mapstructMapper.districtModelToDistrictRqDTO(districtModelRqDTO)));

        return new ResponseEntity<>(districtResponse, HttpStatus.CREATED);

    }

    @GetMapping("/retrieve")
    public ResponseEntity<List<DistrictModelRsDTO>> retrieveAllDistrict () {

        List<DistrictModelRsDTO> districtListResponse = mapstructMapper.mapAllDistrictModel(districtUseCase.retrieveAllDistrict());
        return new ResponseEntity<>(districtListResponse, HttpStatus.OK);

    }

}
