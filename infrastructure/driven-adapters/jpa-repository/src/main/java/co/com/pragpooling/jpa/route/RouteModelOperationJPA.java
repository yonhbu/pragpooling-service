package co.com.pragpooling.jpa.route;

import co.com.pragpooling.jpa.util.MapStructMapperJPA;
import co.com.pragpooling.model.route.RouteModel;
import co.com.pragpooling.model.route.gateways.IRouteModelGateway;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Component
@RequiredArgsConstructor
public class RouteModelOperationJPA implements IRouteModelGateway {

    private final IRouteModelRepositoryJPA iRouteModelRepositoryJPA;
    private final MapStructMapperJPA mapstructMapper;


    @Override
    @Transactional(readOnly = true)
    public List<RouteModel> retrieveAllRoute() {

        return mapstructMapper.mapAllRouteModel(iRouteModelRepositoryJPA.findAll());

    }


}

