package co.com.pragpooling.jpa.journey;


import co.com.pragpooling.jpa.util.MapStructMapperJPA;
import co.com.pragpooling.model.journey.JourneyModel;
import co.com.pragpooling.model.journey.gateways.IJourneyModelGateway;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Component
@RequiredArgsConstructor
public class JourneyOperationJPA implements IJourneyModelGateway {

	private final IJourneyRepositoryJPA iJourneyRepositoryJPA;
	private final MapStructMapperJPA mapstructMapper;


	@Override
	public JourneyModel saveJourneyDriver (JourneyModel journeyModel) {

		Integer countNumberStops = journeyModel.getRoute().getDistrictList().size();
		journeyModel.setNumberStops(countNumberStops);

		return mapstructMapper.journeyModelToJourneyEntity(iJourneyRepositoryJPA.save(
															mapstructMapper.journeyEntityToJourneyModel(journeyModel)));

	}

	@Override
	@Transactional(readOnly = true)
	public List<JourneyModel> retrieveAllJourneyDriver() {

		return mapstructMapper.mapAllJourneyModel(iJourneyRepositoryJPA.findAll());

	}

}
