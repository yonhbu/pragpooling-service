package co.com.pragpooling.jpa.route;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface IRouteModelRepositoryJPA extends CrudRepository<RouteModelEntity, Long> {


}
