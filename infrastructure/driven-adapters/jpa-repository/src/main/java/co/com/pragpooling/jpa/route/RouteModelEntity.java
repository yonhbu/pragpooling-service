package co.com.pragpooling.jpa.route;


import co.com.pragpooling.jpa.district.DistrictEntityJPA;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Set;


@Getter
@Setter
@RequiredArgsConstructor
@Builder(toBuilder = true)
@AllArgsConstructor
@Entity
@Table(name="ROUTE")
public class RouteModelEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_route")
    @Id
    private Long idRoute;

    @Column(name = "description")
    @NotBlank(message = "should not be empty")
    private String description;

    @Column(name = "spaces_available")
    private int spacesAvailable;

    @Column(name = "id_driver")
    private Long idDriver;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "route_district",
               joinColumns = { @JoinColumn (name = "id_route")},
               inverseJoinColumns = {@JoinColumn (name = "id_district")})
    private Set<DistrictEntityJPA> districtList;

}
