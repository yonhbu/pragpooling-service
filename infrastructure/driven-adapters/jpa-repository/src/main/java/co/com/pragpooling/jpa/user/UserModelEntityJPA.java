package co.com.pragpooling.jpa.user;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

@Getter
@Setter
@RequiredArgsConstructor
@Builder(toBuilder = true)
@AllArgsConstructor
@Entity
@Table(name="USERDATA")
public class UserModelEntityJPA implements Serializable {

	private static final long serialVersionUID = 1L;

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column (name = "id_user")
	@Id
	private Long idUser;

	@NotBlank(message = "should not be empty")
	private String name;

	@Column(unique=true)
	@NotBlank(message = "should not be empty")
	@Email(regexp = "[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}",
			message = "Please enter a valid e-mail address")
	private String email;

	@NotBlank(message = "should not be empty")
	private String familyName;

	@NotBlank(message = "should not be empty")
	private String address;

	@NotBlank(message = "should not be empty")
	private String phoneNumber;

	@NotBlank(message = "password is mandatory")
	@Pattern(regexp = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[*_-])(?=\\S+$).{8,15}$",
			  message = "Please enter a valid password: The characteristics of the password must be:" +
					  " a minimum length of 8 characters and a maximum of 15, it must contain uppercase lowercase letters" +
					  ", numbers and a character such as *_-")
	private String password;


}
