package co.com.pragpooling.jpa.util;


import co.com.pragpooling.jpa.district.DistrictEntityJPA;
import co.com.pragpooling.jpa.journey.JourneyModelEntity;
import co.com.pragpooling.jpa.route.RouteModelEntity;
import co.com.pragpooling.jpa.user.UserModelEntityJPA;
import co.com.pragpooling.model.district.DistrictModel;
import co.com.pragpooling.model.journey.JourneyModel;
import co.com.pragpooling.model.route.RouteModel;
import co.com.pragpooling.model.user.UserModel;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;
import java.util.Optional;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE,
        componentModel = "spring"
)
public interface MapStructMapperJPA {


    UserModelEntityJPA userEntityToUserModel (UserModel userModel);
    UserModel userModelToUserEntity (UserModelEntityJPA userModelEntityJPA);

    UserModel userModelToUserEntityWithOptional (Optional<UserModelEntityJPA> userModelEntityJPA);
    List<UserModel> mapAllUserModel (Iterable<UserModelEntityJPA> userModelEntityJPA);

    List<RouteModel> mapAllRouteModel (Iterable<RouteModelEntity> routeModelEntity);

    DistrictEntityJPA districtEntityToDistrictModel (DistrictModel districtModel);
    DistrictModel districtModelToDistrictEntity (DistrictEntityJPA districtEntityJPA);
    List<DistrictModel> mapAllDistrictModel (Iterable<DistrictEntityJPA> districtEntityJPA);

    JourneyModelEntity journeyEntityToJourneyModel (JourneyModel journeyModel);
    JourneyModel journeyModelToJourneyEntity (JourneyModelEntity journeyModelEntity);
    List<JourneyModel> mapAllJourneyModel (Iterable<JourneyModelEntity> journeyModelEntity);


}