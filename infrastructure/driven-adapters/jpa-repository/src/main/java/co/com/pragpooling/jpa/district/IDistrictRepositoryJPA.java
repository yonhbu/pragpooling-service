package co.com.pragpooling.jpa.district;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface IDistrictRepositoryJPA extends CrudRepository<DistrictEntityJPA, Long> {

        boolean existsByName (String name);

}
