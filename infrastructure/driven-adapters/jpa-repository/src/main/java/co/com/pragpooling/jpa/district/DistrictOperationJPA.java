package co.com.pragpooling.jpa.district;


import co.com.pragpooling.jpa.util.MapStructMapperJPA;
import co.com.pragpooling.model.district.DistrictModel;
import co.com.pragpooling.model.district.gateways.IDistrictModelGateway;
import co.com.pragpooling.model.error.DistrictDuplicateException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Component
@RequiredArgsConstructor
public class DistrictOperationJPA implements IDistrictModelGateway {

	private final IDistrictRepositoryJPA iDistrictRepositoryJPA;
	private final MapStructMapperJPA mapstructMapper;


	@Override
	public DistrictModel saveDistrict(DistrictModel districtModel) {

		validateDuplicateDistrict(districtModel);
		return mapstructMapper.districtModelToDistrictEntity(iDistrictRepositoryJPA.save(mapstructMapper.districtEntityToDistrictModel(districtModel)));

	}



	@Override
	@Transactional(readOnly = true)
	public List<DistrictModel> retrieveAllDistrict() {

		return mapstructMapper.mapAllDistrictModel(iDistrictRepositoryJPA.findAll());

	}



	public void validateDuplicateDistrict (DistrictModel districtModelValidate) {

		DistrictEntityJPA districtRequestJPA = 	mapstructMapper.districtEntityToDistrictModel(districtModelValidate);

		String nameDistrict = districtRequestJPA.getName();

		if(iDistrictRepositoryJPA.existsByName(nameDistrict)) {
			throw new DistrictDuplicateException();
		}

	}

}
