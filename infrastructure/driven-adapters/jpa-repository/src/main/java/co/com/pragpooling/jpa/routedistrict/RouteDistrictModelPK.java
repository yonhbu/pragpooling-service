package co.com.pragpooling.jpa.routedistrict;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;


@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Builder(toBuilder = true)
@AllArgsConstructor
@Embeddable
public class RouteDistrictModelPK implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "id_routes")
    private Long idRoutes;

    @Column (name = "id_districts")
    private Long idDistricts;




}
