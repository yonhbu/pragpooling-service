package co.com.pragpooling.jpa.user;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface IUserModelRepositoryJPA extends CrudRepository<UserModelEntityJPA, Long> {

   UserModelEntityJPA  findByEmail(String email);

		
}
