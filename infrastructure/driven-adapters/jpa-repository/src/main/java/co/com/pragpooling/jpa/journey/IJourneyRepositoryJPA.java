package co.com.pragpooling.jpa.journey;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface IJourneyRepositoryJPA extends CrudRepository<JourneyModelEntity, Long> {

}
