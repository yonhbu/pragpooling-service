package co.com.pragpooling.jpa.user;


import co.com.pragpooling.jpa.util.MapStructMapperJPA;
import co.com.pragpooling.model.error.EmailNotFoundException;
import co.com.pragpooling.model.error.ResourceNotFoundException;
import co.com.pragpooling.model.user.UserModel;
import co.com.pragpooling.model.user.gateways.IUserModelGateway;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Component
@RequiredArgsConstructor
public class UserModelOperationJPA implements IUserModelGateway {

	private final IUserModelRepositoryJPA iUserModelRepositoryJPA;
	private final MapStructMapperJPA mapstructMapper;


	@Override
	@Transactional(readOnly = true)
	public List<UserModel> retrieveAllUser() {

		return mapstructMapper.mapAllUserModel(iUserModelRepositoryJPA.findAll());

	}

	@Override
	@Transactional(readOnly = true)
	public UserModel getUserByEmail(String email) {

		UserModel userModelResponse = mapstructMapper.userModelToUserEntity(iUserModelRepositoryJPA.findByEmail(email));

		if (userModelResponse == null) {
			throw new EmailNotFoundException();
		}

		return userModelResponse;
	}


	@Override
	@Transactional(readOnly = true)
	public UserModel findUserById(Long idDriverValidate) {

		Optional<UserModel> userResponse = Optional.ofNullable(mapstructMapper.userModelToUserEntityWithOptional(
				iUserModelRepositoryJPA.findById(idDriverValidate)));

		if (!userResponse.isPresent()) {
			throw new ResourceNotFoundException();
		}

		return userResponse.get();
	}

}
