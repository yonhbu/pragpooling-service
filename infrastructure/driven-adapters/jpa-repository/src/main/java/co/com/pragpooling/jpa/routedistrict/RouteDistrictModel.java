package co.com.pragpooling.jpa.routedistrict;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import java.io.Serializable;


@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Builder(toBuilder = true)
@Transactional(readOnly = true)
@AllArgsConstructor
@Table(name="ROUTE_DISTRICT")
public class RouteDistrictModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private RouteDistrictModelPK id;

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_route_district")
    @Id
    private Long idRouteDistrict;


    @ManyToOne
    @MapsId("id_routes")
    @JoinColumn (name = "id_routes", insertable = false, updatable = false)
    @Column(name = "id_route")
    private Long idRoute;


    @ManyToOne
    @JoinColumn(name = "id_districts", insertable = false, updatable = false)
    @Column (name = "id_district")
    private Long idDistrict;

    @Column (name = "meeting_point")
    private String meetingPoint;

    @Column (name = "location")
    private int location;



}
