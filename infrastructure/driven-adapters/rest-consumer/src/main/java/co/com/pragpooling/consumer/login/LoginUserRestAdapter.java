package co.com.pragpooling.consumer.login;


import co.com.pragpooling.model.auth.LoginUserModel;
import co.com.pragpooling.model.auth.TokenAuthenticateUserModel;
import co.com.pragpooling.model.auth.gateways.ILoginUserGateway;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;


@Component
@RequiredArgsConstructor
public class LoginUserRestAdapter implements ILoginUserGateway {

	@Value("${adapter.rest.consumer.url.login}")
	private String urlCognito;

	private final RestTemplate restTemplate;


	@Override
	public TokenAuthenticateUserModel loginUser(LoginUserModel loginUserModel) {

		return restTemplate.postForObject(urlCognito, loginUserModel, TokenAuthenticateUserModel.class);
	}


}


