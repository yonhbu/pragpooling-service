package co.com.pragpooling.consumer.signup;


import co.com.pragpooling.jpa.user.IUserModelRepositoryJPA;
import co.com.pragpooling.jpa.user.UserModelEntityJPA;
import co.com.pragpooling.model.auth.SignUpUserModel;
import co.com.pragpooling.model.auth.SignUpStatusUserModel;
import co.com.pragpooling.model.auth.gateways.ISignUpUserGateway;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import org.springframework.web.client.RestTemplate;


@Component
@RequiredArgsConstructor
public class SignUpRestAdapter implements ISignUpUserGateway {

	@Value("${adapter.rest.consumer.url.signup}")
	private String urlCognito;

	private final RestTemplate restTemplate;
	private final IUserModelRepositoryJPA iUserModelRepositoryJPA;


	@Override
	public SignUpStatusUserModel signUpUser(SignUpUserModel signUpUserModel) {

		SignUpStatusUserModel signUpStatusUserModel = restTemplate.postForObject(urlCognito, signUpUserModel,
																					 SignUpStatusUserModel.class);
		String userToken = getSplitResponseUrlCognito(signUpStatusUserModel);

		UserModelEntityJPA signUpUserDataJPA = new UserModelEntityJPA();
		signUpUserDataJPA.setName(userToken);
		signUpUserDataJPA.setEmail(signUpUserModel.getEmail());
		signUpUserDataJPA.setFamilyName(signUpUserModel.getFamilyName());
		signUpUserDataJPA.setAddress(signUpUserModel.getAddress());
		signUpUserDataJPA.setPhoneNumber(signUpUserModel.getPhoneNumber());
		signUpUserDataJPA.setPassword(signUpUserModel.getPassword());

		iUserModelRepositoryJPA.save(signUpUserDataJPA);

		return signUpStatusUserModel;
	}



	public String getSplitResponseUrlCognito (SignUpStatusUserModel signUpStatusUserModel) {

		String getResponseBody = signUpStatusUserModel.getBody();


		String unquoted = getResponseBody.replace("\"", " ");
		String[] splitFirstPartResponseBody = unquoted.split(":");

		String reduceStringSize = splitFirstPartResponseBody[splitFirstPartResponseBody.length-2];

		String[] splitSecondPartResponseBody = reduceStringSize.split(",");
		return splitSecondPartResponseBody[splitSecondPartResponseBody.length-2];

	}



}


