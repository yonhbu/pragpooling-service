package com.pragpooling.helpers.handler.exception.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class MessageErrorDTO {
	
    private Date timestamp;
	private String status;
	private String message;
	private String details;
}
