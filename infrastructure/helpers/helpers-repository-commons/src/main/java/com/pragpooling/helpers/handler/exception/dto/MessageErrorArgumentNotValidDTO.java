package com.pragpooling.helpers.handler.exception.dto;

import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.List;

@Data
public class MessageErrorArgumentNotValidDTO {

	private final int status;
	private final String error;
	private final String message;
	private List<String> detailedMessages;

	public MessageErrorArgumentNotValidDTO(HttpStatus httpStatus, String message) {
		status = httpStatus.value();
		error = httpStatus.getReasonPhrase();
		this.message = message;
	}

}
