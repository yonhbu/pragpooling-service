package co.com.pragpooling.config;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertNotNull;


public class UseCasesConfigTest {

        @InjectMocks
        private UseCaseConfig useCaseConfig;

        @Before
        public void setUp() {
                MockitoAnnotations.initMocks(this);
        }

        @Test
        public void objectMapperShouldNotNull() {
                assertNotNull(useCaseConfig.objectMapper());
        }

        @Test
        public void globalExceptionHandlerShouldNotNull() {
                assertNotNull(useCaseConfig.globalExceptionHandler());
        }

        @Test
        public void restTemplateShouldNotNull() {
                assertNotNull(useCaseConfig.restTemplate());
        }
}
