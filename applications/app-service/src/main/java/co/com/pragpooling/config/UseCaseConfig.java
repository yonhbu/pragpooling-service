package co.com.pragpooling.config;

import com.pragpooling.helpers.handler.exception.GlobalExceptionHandler;
import org.reactivecommons.utils.ObjectMapperImp;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.web.client.RestTemplate;


@Configuration
@ComponentScan(basePackages = {"co.com.pragpooling.usecase",
                               "co.com.pragpooling.helpers",
                               },
        includeFilters = {
                @ComponentScan.Filter(type = FilterType.REGEX, pattern = "^.+UseCase$")
        },
        useDefaultFilters = false)
public class UseCaseConfig {


        @Bean
        public ObjectMapperImp objectMapper() {
                return new ObjectMapperImp();
        }

        @Bean
        public GlobalExceptionHandler globalExceptionHandler () {
                return new GlobalExceptionHandler();
        }

        @Bean
        public RestTemplate restTemplate() {
                return new RestTemplate();
        }


}
