package co.com.pragpooling.usecase.user;

import co.com.pragpooling.model.user.UserModel;
import co.com.pragpooling.model.user.gateways.IUserModelGateway;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class UserUseCase {

    private final IUserModelGateway iUserModelGateway;

        public List<UserModel> retrieveAllUser () {
        return  iUserModelGateway.retrieveAllUser();
    }

    public UserModel getUserByEmail (String email) {
        return iUserModelGateway.getUserByEmail(email);
    }


}
