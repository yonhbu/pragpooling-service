package co.com.pragpooling.usecase.journey;

import co.com.pragpooling.model.district.DistrictModel;
import co.com.pragpooling.model.error.ResourceNotFoundException;
import co.com.pragpooling.model.error.ValidateMinimumNumberDistrict;
import co.com.pragpooling.model.error.ValidateSpaceAvailable;
import co.com.pragpooling.model.journey.JourneyModel;
import co.com.pragpooling.model.journey.gateways.IJourneyModelGateway;
import co.com.pragpooling.model.user.UserModel;
import co.com.pragpooling.model.user.gateways.IUserModelGateway;
import lombok.RequiredArgsConstructor;
import java.util.List;
import java.util.Set;


@RequiredArgsConstructor
public class JourneyUseCase {

    private final IJourneyModelGateway iJourneyModelGateway;
    private final IUserModelGateway iUserModelGateway;


    public JourneyModel saveJourneyDriver (JourneyModel journeyModel) {

        Long idDriverRetrieve = journeyModel.getRoute().getIdDriver();

        validateRequestInfo (idDriverRetrieve);
        validateMinimumNumberDistrict(journeyModel);
        validateSpaceAvailable(journeyModel);

        return iJourneyModelGateway.saveJourneyDriver(journeyModel);
    }


    public List<JourneyModel> retrieveAllJourneyDriver () {
        return  iJourneyModelGateway.retrieveAllJourneyDriver();

    }



    public void validateRequestInfo (Long idDriverValidate) {
        UserModel userResponse = iUserModelGateway.findUserById(idDriverValidate);

        if (userResponse == null) {
            throw new ResourceNotFoundException();
        }

    }


    public void validateMinimumNumberDistrict(JourneyModel journeyModel) {

       Set<DistrictModel> listValuesDistrictRequest = journeyModel.getRoute().getDistrictList();

        boolean validateMinimumNumberDistrict = (long) listValuesDistrictRequest.size() >= 2;
        if (!validateMinimumNumberDistrict) {
            throw new ValidateMinimumNumberDistrict();
        }

    }

    public void validateSpaceAvailable(JourneyModel journeyModel) {

        int spaceAvailableRequest = journeyModel.getRoute().getSpacesAvailable();
        if (spaceAvailableRequest < 1 || spaceAvailableRequest > 4) {
            throw new ValidateSpaceAvailable();
        }

    }


}
