package co.com.pragpooling.usecase.auth;


import co.com.pragpooling.model.auth.SignUpUserModel;
import co.com.pragpooling.model.auth.SignUpStatusUserModel;
import co.com.pragpooling.model.auth.gateways.ISignUpUserGateway;

import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public class SignUpUserUseCase {

	private final ISignUpUserGateway iSignUpUserGateway;

	public SignUpStatusUserModel signUpUser (SignUpUserModel signUpUserModel) {
		return iSignUpUserGateway.signUpUser(signUpUserModel);
	}

}