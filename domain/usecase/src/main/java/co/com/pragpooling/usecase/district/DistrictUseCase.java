package co.com.pragpooling.usecase.district;

import co.com.pragpooling.model.district.DistrictModel;
import co.com.pragpooling.model.district.gateways.IDistrictModelGateway;
import lombok.RequiredArgsConstructor;

import java.util.List;


@RequiredArgsConstructor
public class DistrictUseCase {

    private final IDistrictModelGateway iDistrictModelGateway;


    public DistrictModel saveDistrict (DistrictModel districtModel) {
        return iDistrictModelGateway.saveDistrict(districtModel);
    }


    public List<DistrictModel> retrieveAllDistrict () {

        return iDistrictModelGateway.retrieveAllDistrict();

    }



}
