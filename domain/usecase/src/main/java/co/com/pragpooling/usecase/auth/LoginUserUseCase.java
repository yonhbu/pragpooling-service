package co.com.pragpooling.usecase.auth;


import co.com.pragpooling.model.auth.LoginUserModel;
import co.com.pragpooling.model.auth.TokenAuthenticateUserModel;
import co.com.pragpooling.model.auth.gateways.ILoginUserGateway;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public class LoginUserUseCase {

	private final ILoginUserGateway iloginUserGateway;

	public TokenAuthenticateUserModel loginUser(LoginUserModel loginUserModel) {
				return  iloginUserGateway.loginUser(loginUserModel);
	}


}