package co.com.pragpooling.usecase.route;

import co.com.pragpooling.model.route.RouteModel;
import co.com.pragpooling.model.route.gateways.IRouteModelGateway;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class RouteUseCase {

    private final IRouteModelGateway iRouteModelGateway;


    public List<RouteModel> retrieveAllRoute () {
        return  iRouteModelGateway.retrieveAllRoute();
    }

}
