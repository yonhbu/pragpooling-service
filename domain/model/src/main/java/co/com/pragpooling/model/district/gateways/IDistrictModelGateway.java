package co.com.pragpooling.model.district.gateways;


import co.com.pragpooling.model.district.DistrictModel;
import java.util.List;


public interface IDistrictModelGateway {

   DistrictModel saveDistrict (DistrictModel districtModel);
   List<DistrictModel> retrieveAllDistrict ();

}
