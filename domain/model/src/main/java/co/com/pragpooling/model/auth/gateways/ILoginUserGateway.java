package co.com.pragpooling.model.auth.gateways;


import co.com.pragpooling.model.auth.LoginUserModel;
import co.com.pragpooling.model.auth.TokenAuthenticateUserModel;

public interface ILoginUserGateway {

	TokenAuthenticateUserModel loginUser (LoginUserModel loginUserModel);

}
