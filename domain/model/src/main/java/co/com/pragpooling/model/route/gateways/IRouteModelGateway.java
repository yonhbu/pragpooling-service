package co.com.pragpooling.model.route.gateways;


import co.com.pragpooling.model.route.RouteModel;

import java.util.List;

public interface IRouteModelGateway {

    List<RouteModel> retrieveAllRoute();

}
