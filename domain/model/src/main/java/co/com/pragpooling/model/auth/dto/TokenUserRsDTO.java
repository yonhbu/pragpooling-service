package co.com.pragpooling.model.auth.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class TokenUserRsDTO {

	private int statusCode;
	private String body;
	private boolean base64Encoded;


}

