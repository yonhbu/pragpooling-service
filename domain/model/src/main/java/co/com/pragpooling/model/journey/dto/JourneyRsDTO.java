package co.com.pragpooling.model.journey.dto;

import co.com.pragpooling.model.route.dto.RouteRsDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class JourneyRsDTO {

    private Long idJourney;
    private RouteRsDTO route;
    private Integer numberStops;
    private List<String> schedule;

}
