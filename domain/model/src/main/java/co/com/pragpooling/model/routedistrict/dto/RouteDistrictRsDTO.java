package co.com.pragpooling.model.routedistrict.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class RouteDistrictRsDTO {

	private Long routeDistrict;
	private Long idRoute;
	private Long idDistrict;
	private String meetingPoint;
	private int location;
}
