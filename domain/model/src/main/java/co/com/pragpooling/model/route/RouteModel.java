package co.com.pragpooling.model.route;


import co.com.pragpooling.model.district.DistrictModel;
import lombok.Getter;
import lombok.Setter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Set;


@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RouteModel {

    private Long idRoute;
    private String description;
    private int spacesAvailable;
    private Long idDriver;
    private Set<DistrictModel> districtList;

}
