package co.com.pragpooling.model.error;

public class ValidateMinimumNumberDistrict extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public ValidateMinimumNumberDistrict() {
		super("Minimum number of registered District is two");
	}

}