package co.com.pragpooling.model.routedistrict;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class RouteDistrictModel {

    private Long routeDistrict;
    private Long idRoute;
    private Long idDistrict;
    private String meetingPoint;
    private int location;


}
