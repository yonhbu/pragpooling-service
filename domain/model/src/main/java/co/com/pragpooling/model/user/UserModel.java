package co.com.pragpooling.model.user;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode
@Builder(toBuilder = true)
public class UserModel{

    private Long idUser;
    private String name;
    private String email;
    private String familyName;
    private String address;
    private String phoneNumber;
    private String password;


}
