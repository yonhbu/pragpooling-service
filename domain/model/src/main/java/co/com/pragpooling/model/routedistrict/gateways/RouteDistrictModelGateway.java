package co.com.pragpooling.model.routedistrict.gateways;


import co.com.pragpooling.model.routedistrict.RouteDistrictModel;
import java.util.List;


public interface RouteDistrictModelGateway {

    RouteDistrictModel saveRouteDistrict (RouteDistrictModel routeDistrictModel);
    List<RouteDistrictModel> retrieveAllRouteDistrict();


}
