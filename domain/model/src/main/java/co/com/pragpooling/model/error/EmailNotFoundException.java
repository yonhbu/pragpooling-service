package co.com.pragpooling.model.error;

public class EmailNotFoundException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public EmailNotFoundException() {
		super(" Email Not found ");
	}

}