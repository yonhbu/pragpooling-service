package co.com.pragpooling.model.auth;



import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder(toBuilder = true)
@ToString
public class TokenAuthenticateUserModel {

	private int statusCode;
	private String body;
	private boolean base64Encoded;


}

