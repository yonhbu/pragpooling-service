package co.com.pragpooling.model.auth.gateways;

import co.com.pragpooling.model.auth.SignUpUserModel;
import co.com.pragpooling.model.auth.SignUpStatusUserModel;


public interface ISignUpUserGateway {

	SignUpStatusUserModel signUpUser  (SignUpUserModel signUpUserModel);

}
