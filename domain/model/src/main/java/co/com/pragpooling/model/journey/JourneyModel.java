package co.com.pragpooling.model.journey;


import co.com.pragpooling.model.route.RouteModel;
import lombok.Getter;
import lombok.Setter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class JourneyModel {

    private Long idJourney;
    private RouteModel route;
    private Integer numberStops;
    private List<String> schedule;



}
