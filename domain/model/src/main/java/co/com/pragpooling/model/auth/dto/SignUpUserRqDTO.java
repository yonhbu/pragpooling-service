package co.com.pragpooling.model.auth.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class SignUpUserRqDTO {

	private String name;
	private String email;
	private String familyName;
	private String address;
	private String phoneNumber;
	private String password;


}
