package co.com.pragpooling.model.error;

public class DistrictDuplicateException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public DistrictDuplicateException() {
		super("District Duplicate, Please insert other name");
	}

}