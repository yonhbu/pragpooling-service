package co.com.pragpooling.model.error;

public class ValidateSpaceAvailable extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public ValidateSpaceAvailable() {
		super("The number of available slots must not be empty and must be between 1 and 4.");
	}

}