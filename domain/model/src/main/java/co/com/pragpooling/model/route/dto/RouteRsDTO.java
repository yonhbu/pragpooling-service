package co.com.pragpooling.model.route.dto;

import co.com.pragpooling.model.district.dto.DistrictModelRqDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.util.Set;


@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class RouteRsDTO {

    private Long idRoute;
    private String description;
    private int spacesAvailable;
    private Long idDriver;
    private Set<DistrictModelRqDTO> districtList;

}
