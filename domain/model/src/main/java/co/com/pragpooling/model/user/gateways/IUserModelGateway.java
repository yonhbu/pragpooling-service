package co.com.pragpooling.model.user.gateways;


import co.com.pragpooling.model.user.UserModel;
import java.util.List;


public interface IUserModelGateway {

    List<UserModel> retrieveAllUser();
    UserModel getUserByEmail (String email);
    UserModel findUserById (Long idDriverValidate);

}
