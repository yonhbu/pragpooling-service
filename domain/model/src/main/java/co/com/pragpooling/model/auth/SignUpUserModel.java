package co.com.pragpooling.model.auth;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder(toBuilder = true)
@ToString
public class SignUpUserModel {
	
	private Long idSignup;
	private String name;
	private String email;
	private String familyName;
	private String address;
	private String phoneNumber;
	private String password;


}
