package co.com.pragpooling.model.journey.dto;

import co.com.pragpooling.model.route.dto.RouteRqDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class JourneyRqDTO {

    private RouteRqDTO route;
    private List<String> schedule;

}
