package co.com.pragpooling.model.district.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class DistrictModelRsDTO {

    private Long idDistrict;
    private String name;
    private String description;

}
