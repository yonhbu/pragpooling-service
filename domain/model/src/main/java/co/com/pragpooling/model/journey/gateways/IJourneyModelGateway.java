package co.com.pragpooling.model.journey.gateways;


import co.com.pragpooling.model.journey.JourneyModel;

import java.util.List;

public interface IJourneyModelGateway {

    JourneyModel saveJourneyDriver (JourneyModel journeyModel);
    List<JourneyModel> retrieveAllJourneyDriver ();

}
