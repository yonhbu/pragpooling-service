package co.com.pragpooling.model.user.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;



@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class UserRqDTO {

	private String name;
	private String email;
	private String familyName;
	private String address;
	private String phoneNumber;
	private String password;

}
